//i2cdev mpu6050 code taken from https://playground.arduino.cc/Main/MPU-6050
//ledcode from https://randomnerdtutorials.com/guide-for-ws2812b-addressable-rgb-led-strip-with-arduino/

#include <FastLED.h>
#include <Wire.h>
#define MOVELIMIT 2000

const int MPU_addr=0x68;  // I2C address of the MPU-6050
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ,tot_move = 0,tot_move_old = tot_move;

int ULTrigPin = 3;
int ULEchoPin = 2;
int32_t sleepCounter = 0;
double distance;

int buzzer = 6;

#define LED_PIN     5
#define NUM_LEDS    120
#define BRIGHTNESS  64
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];

#define UPDATES_PER_SECOND 100

CRGBPalette16 currentPalette;
TBlendType    currentBlending;

extern CRGBPalette16 myRedWhiteBluePalette;
extern const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM;

void setup() {
  // put your setup code here, to run once:
	//Serial.begin(9600);
	pinMode(buzzer, OUTPUT);
	
	pinMode(ULTrigPin, OUTPUT);
	pinMode(ULEchoPin, INPUT);
	
	Wire.begin();
	Wire.beginTransmission(MPU_addr);
	Wire.write(0x6B);  // PWR_MGMT_1 register
	Wire.write(0);     // set to zero (wakes up the MPU-6050)
	Wire.endTransmission(true);
	Serial.begin(9600);
	
	Serial.println("Achtung X-mas IoT Project!");
	
	    delay( 3000 ); // power-up safety delay
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.setBrightness(  BRIGHTNESS );
    
    currentPalette = RainbowColors_p;
    currentBlending = LINEARBLEND;
	
	tone(buzzer,500);
	delay(500);
	noTone(buzzer);
	
	randomSeed(analogRead(0));
}

//code shamelessly stolen from
//https://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/
double meassureDistance()
{
	double meassureDistance_distance,duration;
	// Clears the trigPin
	digitalWrite(ULTrigPin, LOW);
	delayMicroseconds(2);
	// Sets the trigPin on HIGH state for 10 micro seconds
	digitalWrite(ULTrigPin, HIGH);
	delayMicroseconds(10);
	digitalWrite(ULTrigPin, LOW);
	// Reads the echoPin, returns the sound wave travel time in microseconds
	duration = pulseIn(ULEchoPin, HIGH);
	// Calculating the distance
	meassureDistance_distance= duration*0.034/2;
	// Prints the distance on the Serial Monitor
	
	if (meassureDistance_distance <= 400 && meassureDistance_distance > 0)
	{
		return meassureDistance_distance;
	}
	else
	{
		meassureDistance_distance = -1.0;
		return meassureDistance_distance;
	};
};

void FillLEDsFromPaletteColors( uint8_t colorIndex)
{
    uint8_t brightness = 255;
    
    for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = ColorFromPalette( currentPalette, colorIndex, brightness, currentBlending);
        colorIndex += 3;
    }
};

void ChangePalettePeriodically()
{
    uint8_t secondHand = (millis() / 1000) % 60;
    static uint8_t lastSecond = 99;
    
    if( lastSecond != secondHand) {
        lastSecond = secondHand;
        if( secondHand ==  0)  { currentPalette = RainbowColors_p;         currentBlending = LINEARBLEND; }
        if( secondHand == 10)  { currentPalette = RainbowStripeColors_p;   currentBlending = NOBLEND;  }
        if( secondHand == 15)  { currentPalette = RainbowStripeColors_p;   currentBlending = LINEARBLEND; }
        if( secondHand == 20)  { SetupPurpleAndGreenPalette();             currentBlending = LINEARBLEND; }
        if( secondHand == 25)  { SetupTotallyRandomPalette();              currentBlending = LINEARBLEND; }
        if( secondHand == 30)  { SetupBlackAndWhiteStripedPalette();       currentBlending = NOBLEND; }
        if( secondHand == 35)  { SetupBlackAndWhiteStripedPalette();       currentBlending = LINEARBLEND; }
        if( secondHand == 40)  { currentPalette = CloudColors_p;           currentBlending = LINEARBLEND; }
        if( secondHand == 45)  { currentPalette = PartyColors_p;           currentBlending = LINEARBLEND; }
        if( secondHand == 50)  { currentPalette = myRedWhiteBluePalette_p; currentBlending = NOBLEND;  }
        if( secondHand == 55)  { currentPalette = myRedWhiteBluePalette_p; currentBlending = LINEARBLEND; }
    }
}

// This function fills the palette with totally random colors.
void SetupTotallyRandomPalette()
{
    for( int i = 0; i < 16; i++) {
        currentPalette[i] = CHSV( random8(), 255, random8());
    }
}

void SetupBlackAndWhiteStripedPalette()
{
    // 'black out' all 16 palette entries...
    fill_solid( currentPalette, 16, CRGB::Black);
    // and set every fourth one to white.
    currentPalette[0] = CRGB::White;
    currentPalette[4] = CRGB::White;
    currentPalette[8] = CRGB::White;
    currentPalette[12] = CRGB::White;
    
}

void SetupPurpleAndGreenPalette()
{
    CRGB purple = CHSV( HUE_PURPLE, 255, 255);
    CRGB green  = CHSV( HUE_GREEN, 255, 255);
    CRGB black  = CRGB::Black;
    
    currentPalette = CRGBPalette16(
                                   green,  green,  black,  black,
                                   purple, purple, black,  black,
                                   green,  green,  black,  black,
                                   purple, purple, black,  black );
}

const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM =
{
    CRGB::Red,
    CRGB::Gray, // 'white' is too bright compared to red and blue
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Red,
    CRGB::Gray,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Blue,
    CRGB::Black,
    CRGB::Black
};

void loop() {
  // put your main code here, to run repeatedly:
	distance = meassureDistance();
	Serial.print("Meassure distance: ");
	Serial.println(distance);
	//delay(1000);
	
	Wire.beginTransmission(MPU_addr);
	Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
	Wire.endTransmission(false);
	Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
	AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)    
	AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
	AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
	Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
	GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
	GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
	GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
	// Serial.print("AcX = "); Serial.print(AcX);
	// Serial.print(" | AcY = "); Serial.print(AcY);
	// Serial.print(" | AcZ = "); Serial.print(AcZ);
	// Serial.print(" | Tmp = "); Serial.print(Tmp/340.00+36.53);  //equation for temperature in degrees C from datasheet
	// Serial.print(" | GyX = "); Serial.print(GyX);
	// Serial.print(" | GyY = "); Serial.print(GyY);
	// Serial.print(" | GyZ = "); Serial.println(GyZ);
	tot_move = 0;
	tot_move = AcX + AcY + AcZ;
	
	if (tot_move - tot_move_old < 2000 || tot_move - tot_move_old < -2000)
	{
		sleepCounter++;
	}
	else
	{
		sleepCounter = 0;
	};
	
	Serial.print("change in accel-data: ");
	Serial.print(tot_move - tot_move_old);
	Serial.print(" ");
	Serial.println(sleepCounter);
	
	tot_move_old = tot_move;
	
	//delay(333);
	
	//3000
	if (sleepCounter > 3000 || (distance <= 30 && distance >= 4))
	{
		Serial.println("ALARM!!!");
		currentPalette = LavaColors_p;
		currentBlending = NOBLEND; 
		tone(buzzer,random(300,1200));
		
		for( int i = 0; i < 16; i++) {
			if (i%2 == 0)
			{
				currentPalette[i] = CHSV( 255, 255, 255);
			}
			else
			{
				currentPalette[i] = CHSV( 0, 0, 0);
			}
		}
		
		//derpTime = 1;
	}
	else
	{
		noTone(buzzer);
		//derpTime = 1000;
		ChangePalettePeriodically();
	};
    
    static uint8_t startIndex = 0;
    startIndex = startIndex + 1; /* motion speed */
    
    FillLEDsFromPaletteColors( startIndex);
    
    FastLED.show();
    FastLED.delay(1000 / UPDATES_PER_SECOND);
}
